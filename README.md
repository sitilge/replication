# replication

A set of trivial scripts for mariadb replacation.

## Setup

Edit the `my.cnf` file (or included files such as `my.cnf.d/server.cnf`) and edit the following lines

````
...
[mariadb]

# replication Master Server (default)
# binary logging is required for replication
log-bin = mysql-bin

# binary logging format - mixed recommended
binlog_format = mixed

# required unique id between 1 and 2^32 - 1
# defaults to 1 if master-host is not set
# but will not function as a master if omitted
server-id = 1

# set to the local IP of the physical NIC
# e.g. 192.168.1.2 if only one bind address is required
# use 0.0.0.0 if more than one bind address is required
bind-address = 192.168.1.2

# set which databases will be replicated (optional)
replicate-do-db = db1
replicate-do-db = db2

# set which databases will be ignored (optional)
replicate-ignore-db = ignore1
replicate-ignore-db = ignore2

...
````

Then, edit the `variables.env` file according to your system and run the `change.sh` script. Master log file and log position can be obtained by running `show master status\G` on the master and then updating the `variables.env` file with the obtained values on the slave. If master-master replacation is required, repeat the steps above for both machines.
