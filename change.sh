#!/bin/sh

source ./variables.env

mysql -u ${user} -p${password} ${db}<<EOFMYSQL
stop slave;

change master to
master_host = "${master_host}",
master_user = "${master_user}",
master_password = "${master_password}",
master_log_file = "${master_log_file}",
master_log_pos = ${master_log_pos}
;

start slave;
EOFMYSQL
